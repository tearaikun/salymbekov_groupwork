import json

def get_user_language(user_id: str):
    with open("db/user.json") as file:
        user_dict = json.loads(file.read())
    return user_dict[user_id]["language"]