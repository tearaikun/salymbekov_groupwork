import json

def delete_user(chat_id):
    with open("db/user.json", "r") as file:
        delete = file.read()
        delete_dict = json.loads(delete)
    
    del delete_dict[str(chat_id)]
    
    with open (file="db/user.json", mode="w") as file:
        file.write(json.dumps(delete_dict))