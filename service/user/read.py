import json

def get_user_language(user_id):
    with open ("db/user.json") as file:
        user_dict = json.loads(file.read())

    return user_dict[str(user_id)]["language"]    
