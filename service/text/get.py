import json

def get_text(text_key, language):
    with open ("db/text.json", "r") as file:
        texts_dict = json.loads(file.read())

    return texts_dict[text_key][language]
