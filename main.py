
import telebot

from telebot.custom_filters import TextFilter, TextMatchFilter, IsReplyFilter
from telebot import types
from keyboards.inline import inlines
from service import user
from service import text



TOKEN = '5744602563:AAGDOGxVabpkkMp5oSQYxiztr-blHL3c6V8'


bot = telebot.TeleBot(TOKEN)


@bot.message_handler(func= lambda message: message.text in ("Hello", "Hi", "hello", "hi", "привет", "Привет", "саламатсызбы", "салам"))
def qwe_handler(message: types.Message):
    bot.send_message(chat_id=message.chat.id, text=message.text)



@bot.message_handler(commands=['start'])
def start_handler(message):
    bot.send_message(chat_id=message.chat.id, text='xnasm', reply_markup=inlines.get_languages())


@bot.callback_query_handler(lambda call: call.data in ("en", "ru", "kg"))
def callback_handler(call):
    if call.data in ("en", "ru", "kg"):
        user.create(
        chat_id=str(call.message.chat.id), 
        first_name=call.message.chat.first_name, 
        last_name=call.message.chat.last_name,
        language=call.data
    )
        
    bot.send_message(chat_id=call.message.chat.id, text=text.get_text("greet", user.get_user_language(str(call.message.chat.id))))    
        

@bot.message_handler(commands=['delete'])
def start_handler(message):
    bot.send_message(chat_id=message.chat.id, text='желаете выйти из записи', reply_markup=inlines.delete_user())        

@bot.callback_query_handler(lambda call: call.data in ("delete_yes", "delete_no"))
def callback_handler(call):
    if call.data == "delete_yes":
        user.delete_user(
        chat_id=str(call.message.chat.id), 
        )
        bot.send_message(chat_id=call.message.chat.id, text="bye-bye!")
    else:
        bot.send_message(chat_id=call.message.chat.id, text="Some text")
        


@bot.message_handler(func=lambda message: message.text in ("send photos"))
def photo_handler(message: types.Message):
    images = []
    for photo in ["media/images/test.jpg", "media/images/test2.jpeg"]:
            image = open(photo, "rb")   
            images.append(types.InputMediaPhoto(image))

    bot.send_media_group(chat_id=message.chat.id, media=images)
    


bot.infinity_polling()    