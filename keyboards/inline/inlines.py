from telebot.types import (
    InlineKeyboardButton,
    InlineKeyboardMarkup
)


def get_languages():
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(
        InlineKeyboardButton(text="English", callback_data="en"),
        InlineKeyboardButton(text="Русский", callback_data="ru"),
        InlineKeyboardButton(text="Кыргызча", callback_data="kg"),
    )
    return keyboard

def delete_user():
    keyboard = InlineKeyboardMarkup(row_width=1)
    keyboard.add(
        InlineKeyboardButton(text="Yes", callback_data="delete_yes"),
        InlineKeyboardButton(text="No", callback_data="delete_no"),
    )
    return keyboard